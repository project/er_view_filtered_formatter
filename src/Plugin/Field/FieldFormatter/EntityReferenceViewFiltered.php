<?php

namespace Drupal\er_view_filtered_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\View;
use Drupal\views\Views;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "er_view_filtered",
 *   label = @Translation("Rendered entity (filtered by view)"),
 *   description = @Translation("Display the referenced entities rendered by entity_view() but filtered by the results of a view"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceViewFiltered extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_name' => '',
      'view_display' => '',
      'arguments' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['view_name'] = [
      '#type' => 'textfield',
      '#title' => t('View name'),
      '#default_value' => $this->getSetting('view_name'),
      '#required' => TRUE,
    ];
    $elements['view_display'] = [
      '#type' => 'textfield',
      '#title' => t('View display'),
      '#default_value' => $this->getSetting('view_display'),
      '#required' => TRUE,
    ];
    $elements['arguments'] = [
      '#type' => 'textfield',
      '#title' => t('Arguments'),
      '#default_value' => $this->getSetting('arguments'),
      '#description' => $this->t('Arguments to pass to the view separated by /'),
      '#required' => TRUE,
    ];
    // Show the token help relevant to this pattern type.
    $elements['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$this->fieldDefinition->get('entity_type')],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $name = $this->getSetting('view_name');
    $display_id = $this->getSetting('view_display');
    if (empty($name) || empty($display_id)) {
      return parent::viewElements($items, $langcode);
    }
    $entity = $items->getEntity();
    $arguments = $this->getArguments($entity);
    $view_result = $this->getViewResult($arguments);
    $filter_callback = function ($item) use ($view_result) {
      $id = $item->entity->id();
      return !empty($view_result[$id]);
    };
    $items->filter($filter_callback);
    $elements = parent::viewElements($items, $langcode);
    return $elements;
  }

  /**
   * Execute the view and get the array of results.
   *
   * @param array $args
   *   The array of arguments processed.
   *
   * @return array|\Drupal\views\ResultRow[]
   *   The array of results.
   */
  public function getViewResult(array $args) {
    $name = $this->getSetting('view_name');
    $display_id = $this->getSetting('view_display');
    $view = Views::getView($name);
    $result = [];
    if (is_object($view)) {
      if (is_array($args)) {
        $view->setArguments($args);
      }
      if (is_string($display_id)) {
        $view->setDisplay($display_id);
      }
      else {
        $view->initDisplay();
      }
      $view->preExecute();
      $view->execute();
      $view_result = [];
      if (empty($view->result) && !empty($view->empty)) {
        foreach ($view->empty as $item) {
          if ($item instanceof View) {
            $output = $item->render();
            $no_result_view = $output['#view'];
            if (!empty($no_result_view->result)) {
              $view_result = $no_result_view->result;
              break;
            }
          }
        }
      }
      else {
        $view_result = $view->result;
      }
      foreach ($view_result as $item) {
        if (empty($item->_entity) || !$item->_entity instanceof ContentEntityInterface) {
          continue;
        }
        $id = $item->_entity->id();
        $result[$id] = $id;
      }
    }
    return $result;
  }

  /**
   * Extract from $entity the tokens values and build the array arguments.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that is rendering the formatter.
   *
   * @return array|null
   *   The array of arguments.
   */
  public function getArguments(ContentEntityInterface $entity) {
    $arguments = $this->getSetting('arguments');
    if (empty($arguments)) {
      return NULL;
    }

    $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
    /** @var \Drupal\token\Token $token */
    $token = \Drupal::service('token');

    $arguments = explode('/', $arguments);
    $return = [];
    foreach ($arguments as $arg) {
      $or = explode('+', $arg);
      $ors = [];
      foreach ($or as $o) {
        $and = explode(',', $o);
        $ands = [];
        foreach ($and as $a) {
          $ands[] = $token->replace($a, [$entity_type => $entity], ['clear' => TRUE]);
        }
        $ors[] = implode(',', $ands);
      }
      $return[] = implode('+', $ors);
    }
    return $return;
  }

}
